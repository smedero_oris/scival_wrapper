from distutils.core import setup

setup(
    # Basic project info
    name='SciVal Wrapper',
    version='1.0.0',
    packages=['scival'],
    
    # Useless metadata cruft
    author='Shawn Medero',
    author_email='smedero@uw.edu',

    url='https://bitbucket.org/smedero_oris/scival_wrapper',
    license='LICENSE.txt',    
    description="SciVal Wrapper is a simple Python interface for SciVal's HTTP API",
    install_requires=[
        "lxml>=3.2.3",
        "python-ntlm>=1.0.1",
        "requests>=1.2.3",
        "requests-ntlm>=0.0.2.3"
    ],
)