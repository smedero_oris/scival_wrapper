# SciVal Wrapper

SciVal Wrapper is a simple Python interface for the SciVal API built around two popular 
Python libraries: Requests (HTTP) and lxml for XML processing.

**Requires Python 2.7.**

## Installation

Ideally you've created a virtualenv and you are using `pip` to manage Python packages.

To install the necessary Python dependencies, use the provided `pip` requirements file:

```
pip install -r requirements.txt
```

## Usage

Pretty standard wrapper fare:

```
import scival
api = scival.Api(base_uri='your_scival_api_uri',
				 username='your_scival_api_username',
				 password='your_scival_api_password')
```

## API Methods

All methods return JSON.  SciVal doesn't have a JSON representation (sigh) but it does 
have a rather simplistic un-namespaced XML one. I made a decision to nudge the XML data 
over into JSON objects. Don't hate me.

### Statistics

#### Usage

```
api.Statistics()
```

### Experts

#### Parameters

* start, positive integer
* end, positive integer (equal or greater to start value)

#### Usage

```
api.Experts(1, 5)
```

### Expert Overview

#### Parameters

* id, a positive integer that is a SciVal ExpertID value

#### Usage

```
api.ExpertOverview(1)
```

### Expert Publications

* id, a positive integer that is a SciVal ExpertID value
* start, positive integer
* end, positive integer (equal or greater to start value)

#### Parameters
#### Usage

```
api.ExpertPublications(1, 1, 5)
```

### Expert Grants

#### Parameters

* id, a positive integer that is a SciVal ExpertID value
* max, limits the number of SciVal Concepts returned

#### Usage

```
api.ExpertGrants(1, 5)
```

### Expert Profiles

#### Parameters

* id, a positive integer that is a SciVal ExpertID value
* max, limits the number of SciVal Concepts returned

#### Usage

```
api.ExpertProfiles(1, 5)
```

### Expert Profile

#### Parameters

* id, a positive integer that is a SciVal ExpertID value
* profile, a string that is the value of a SciVal vocabulary profile id
* max, limits the number of SciVal Concepts returned

#### Usage

```
api.ExpertProfile(1, "msh", 5)
```

### Expert CoAuthors

#### Parameters

* id, a positive integer that is a SciVal ExpertID value
* limit, a string value of either "intern" (limit to "Internal Authors") or "extern" (limit to "External Authors")

#### Usage

```
api.ExpertCoAuthors(1, "intern")
```

### Grants

#### Parameters

* start, positive integer
* end, positive integer (equal or greater to start value)

#### Usage

```
api.Grants(1,5)
```