from lxml import objectify
try: import simplejson as json
except ImportError: import json


# via: http://code.activestate.com/recipes/466341-guaranteed-conversion-to-unicode-or-byte-string/
def safe_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)


def iterNodes(node, parentDict):
    nodeDict = {}
    
    try:
        nodeDict.update(node.attrib)
    except AttributeError:
        pass
        
    if node.text != None:
       nodeDict = node.text
    
    for i in node.iterchildren():
        childDict = {}
        newDict = {}
        newDict = iterNodes(i, childDict)
        newList = []
        if i.tag in nodeDict:
            try:
                nodeDict[i.tag].append(newDict[i.tag])
            except:
                newList.append(nodeDict[i.tag])
                nodeDict[i.tag] = newList
                nodeDict[i.tag].append(newDict[i.tag])
        else:
            nodeDict.update(newDict)
    tagList = node.tag.split(':')
    namespace = '$'.join(tagList)
    parentDict[namespace] = nodeDict
    return parentDict

def parseXMLintoJSON(xml, sort_keys=False, indent=4):
    root = objectify.fromstring(xml)    
    emptyDict = {}
    parsedDict = iterNodes(root, emptyDict)
    return json.dumps(parsedDict, sort_keys, indent)
