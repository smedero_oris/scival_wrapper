#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import json
import urlparse
import requests
from requests_ntlm import HttpNtlmAuth
from scival.utils import parseXMLintoJSON, safe_unicode


class Api(object):
    '''A simple python interface for the SciVal API.'''
    
    def __init__(self,
                 base_uri=None,
                 username=None,
                 password=None,
                 requests_timeout=30,
                 output_json=True):
        '''Instantiate a new scival.Api object.
        
        Args:
            base_uri:
                Your SciVal instance's Restful API URI.
            username:
                Your SciVal API username.
            password:
                Your SciVal API password.
            requests_timeout:
                Set timeout (in seconds) of the http/https requests. Defaults to 30.
        '''
        self.base_uri=base_uri
        self.username=username
        self.password=password
        self.output_json=output_json        
        self.requests_timeout=requests_timeout
        self._service_encoding='utf-8-sig'


    def Statistics(self):
        '''
        Retrieve an overview of entity (experts, publications, grants, last updated date) 
        counts from your SciVal instance.
        '''
        uri = '%s/statistics' % self.base_uri
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
    
    
    def Experts(self, start=None,end=None):
        '''
        Retrieve a list of experts.
        
        Args:
            start: This value helps facilitate a ranged search and is the number to 
                   start form. You must provide both a starting and ending value.
            end: This value helps facilitate a ranged search and is the number to end 
                 with. You must provide both a starting and ending value.            
        '''
        if start is None and end is None:
            uri = '%s/experts' % self.base_uri
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource
        
        try:
            self._is_valid_range(start, end)
            uri = '%s/experts/%d-%d' % (self.base_uri, start, end)
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource
        except:
            raise
    
    
    def ExpertOverview(self, id):
        uri = '%s/expert/%d/overview' % (self.base_uri, id)
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
        
    
    def ExpertPublications(self, id, start=None, end=None, max=None):
        if start is None and end is None:
            uri = '%s/expert/%d/publications' % (self.base_uri, id)
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource
        try:
            self._is_valid_range(start, end)
            uri = '%s/expert/%d/publications/%d-%d/%d' % (self.base_uri, id, start, end, max)
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource            
        except:
            raise
        
    def ExpertGrants(self, id, max=None):
        uri = '%s/expert/%d/grants/%d' % (self.base_uri, id, max)
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
    
    
    def ExpertProfiles(self, id, max=None):
        uri = '%s/expert/%d/profile/%d' % (self.base_uri, id, max)
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
        
        
    def ExpertProfile(self, id, profile, max=None):
        uri = '%s/expert/%d/profile/%s/%d' % (self.base_uri, id, profile, max)
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
    
        
    def ExpertCoAuthors(self, id, limit=None):
        # limit = intern/extern
        
        if limit is not None:
            if limit is not 'intern' or limit is not 'extern':
                raise ValueError('The limit value must be either the string "intern" for \
                internal CoAuthors or "extern" for external CoAuthors.')
        
        uri = '%s/expert/%d/CoAuthors/%s' % (self.base_uri, id, limit)
        resource = self._GetUri(uri, to_json=self.output_json)
        return resource
    
        
    def Grants(self, start=None, end=None):
        '''
        Retrieve a list of grants.
        
        Args:
            start: This value helps facilitate a ranged search and is the number to 
                   start form. You must provide both a starting and ending value.
            end: This value helps facilitate a ranged search and is the number to end 
                 with. You must provide both a starting and ending value.            
        '''    
        if start is None and end is None:
            uri = '%s/grants' % self.base_uri
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource    
    
        try:
            self._is_valid_range(start, end)
            uri = '%s/grants/%d-%d' % (self.base_uri, start, end)
            resource = self._GetUri(uri, to_json=self.output_json)
            return resource
        except:
            raise
    
    
    def _is_valid_range(self, start, end):
        if not isinstance(start, int) and not isinstance(end, int):
            raise ValueError('You must provide both start and end values and they must \
                              be positive numbers.')
        
        if not start > 0:
            raise ValueError('Your start value must be greater than or equal to 1.')
        
        if not end >= start:
            raise ValueError('Your end value must be greater than or equal to your start \
                              value.')
        return True     
    
    def _UriEncodeParams(self, params):
        '''Returns a string in key=value&key=value form ready for use with a valid uri.
        
        Args:
            parameters:
                A dict of (key, value) tuples, where value is encoded as specified by 
                self._encoding
        '''
        if params is None:
            return None
        else:
            return urllib.urlencode(dict([(k, self._Encode(v)) for k, v in params.items() if v is not None]))


    def _BuildUri(self, params=None):
        '''Make it easy to tack query parameters on to a provided uri string'''
        (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(url)
        
        if params and len(params) > 0:
            extra_query = self._UriEncodeParams(params)
            # Add it to the existing query
            if query:
                query += '&' + extra_query
            else:
                query = extra_query
        
        return urlparse.urlunparse((scheme, netloc, path, params, query, fragment))
        
    
    def _GetUri(self, uri, params=None, to_json=True):
        '''Simplify routine GET request for this service.'''
        response = requests.get(
            uri,
            auth=HttpNtlmAuth('scival\\%s' % self.username, self.password),
            timeout=self.requests_timeout
        )
        response.encoding = self._service_encoding
        xml = response.text
        if to_json is True:
            return parseXMLintoJSON(response.text.encode('utf-8'))
        else:
            return xml
